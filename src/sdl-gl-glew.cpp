#include <iostream>

#include <GL/glew.h>
#include <GL/gl.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


enum ErrorCode {
  ERR_SDL_INIT_FAILED = 1,
  ERR_WINDOW_INIT_FAILED,
  ERR_GL_CONTEXT_INIT_FAILED,
  ERR_GL_ERR_ON_CONTEXT_CREATION,
  ERR_GL_SHADER_COMPILATION_FAILED,
  ERR_GL_PROGRAM_CREATION_FAILED,
  ERR_GL_PROGRAM_LINK_FAILED,
  ERR_GLEW_INIT_FAILED,
  ERR_GL_CONFIG_FAILED,
};


SDL_Window* g_pWindow = nullptr;
void destroyWindow() {
  if (g_pWindow) {
    SDL_DestroyWindow(g_pWindow);
    g_pWindow = nullptr;
  }
}


SDL_GLContext g_pGLContext = nullptr;
void destroyContext() {
  if (g_pGLContext) {
    SDL_GL_DeleteContext(g_pGLContext);
    g_pGLContext = nullptr;
  }
}


/** A function which can retrieve the length of a program/shader log. */
typedef void (*log_length_getter)(GLuint id, GLenum what, GLint* pLength);


/** A function which can retrieve a program/shader log. */
typedef void (*log_getter)(
	GLuint id, GLint length, GLsizei* pLength, GLchar* pInfoLog);


std::string
getLog(GLuint id, log_length_getter getLogLength, log_getter getLogContent) {
  GLint length = 0;
  getLogLength(id, GL_INFO_LOG_LENGTH, &length);
  std::string log(length, '\0');
  if (length == 0) {
    return log;
  }
  getLogContent(id, length, nullptr, &log[0]);
  return log;
}


bool
checkGLErrors(char const* const pFile, unsigned long line) {
  bool foundErrors = false;
  GLenum error;
  while ((error = glGetError())) {
    std::cerr << "GL error [" << pFile << ':' << line << "] "
      << glewGetErrorString(error) << std::endl;
    foundErrors = true;
  }
  return foundErrors;
}


#define HAS_GL_ERROR checkGLErrors(__FILE__, __LINE__)


struct HandleResult {
  GLuint handle;
  int error;
};


HandleResult
compileShader(GLenum type, std::string const& source) {
  auto shader = glCreateShader(type);
  {
    auto const* const pSrc = reinterpret_cast<GLchar const*>(source.c_str());
    auto const len = GLint(source.size());
    glShaderSource(shader, 1, &pSrc, &len);
  }
  glCompileShader(shader);
  GLint compiled = GL_FALSE;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
  if (auto const log{getLog(shader, glGetShaderiv, glGetShaderInfoLog)}; 
      !log.empty())
  {
    std::cout << "---- Shader log ----\n" << log << "\n---- End of log ----"
      << std::endl;
  }
  if (HAS_GL_ERROR) {
    glDeleteShader(shader);
    return {0, ERR_GL_SHADER_COMPILATION_FAILED};
  }
  return {shader, 0};
}


HandleResult
loadProgram(
  char const* const pVertShaderSrc,
  char const* const pFragShaderSrc)
{
  // Load vertex shader.
  auto const vertShader = compileShader(GL_VERTEX_SHADER, pVertShaderSrc);
  if (vertShader.error) {
    std::cerr << "Failed to compile vertex shader!" << std::endl;
    glDeleteShader(vertShader.handle);
    return {0, vertShader.error};
  }

  // Load fragment shader.
  auto const fragShader = compileShader(GL_FRAGMENT_SHADER, pFragShaderSrc);
  if (fragShader.error) {
    std::cerr << "Failed to compile fragment shader!" << std::endl;
    glDeleteShader(vertShader.handle);
    return {0, fragShader.error};
  }

  // Create program. Attach shaders and mark for deletion.
  auto const program = glCreateProgram();
  glAttachShader(program, vertShader.handle);
  glAttachShader(program, fragShader.handle);
  glDeleteShader(vertShader.handle);
  glDeleteShader(fragShader.handle);
  if (HAS_GL_ERROR) {
    glDeleteProgram(program);
    return {0, ERR_GL_PROGRAM_CREATION_FAILED};
  }

  // Link program.
  glLinkProgram(program);
  if (auto const log{getLog(program, glGetProgramiv, glGetProgramInfoLog)};
      !log.empty())
  {
    std::cout << "---- Program log ----\n" << log << "\n---- End of log ----"
      << std::endl;
  }
  GLint linked = GL_FALSE;
  glGetProgramiv(program, GL_LINK_STATUS, &linked);
  if (linked != GL_TRUE || HAS_GL_ERROR) {
    std::cerr << "Failed to link program!" << std::endl;
    glDeleteProgram(program);
    return {0, ERR_GL_PROGRAM_LINK_FAILED};
  }

  return {program, 0};
}


constexpr char const* const pVertShaderSrc = R"GLS(
#version 330 core
layout (location = 0) in vec2 position;
uniform mat4 mvp;
out vec2 xy;

void main() {
  gl_Position = mvp * vec4(position.xy, 0.0, 1.0);
  xy = position;
}
)GLS";


constexpr char const* const pFragShaderSrc = R"GLS(
#version 330 core
in vec2 xy;
out vec4 fragColour;

void main() {
  fragColour = vec4(xy.xy, xy.x * xy.y, 1.0);
}
)GLS";


constexpr GLsizei g_numVertices = 6;


void
render(GLuint vao) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  // Technically unnecessary for such a simple example, as the VAO is never
  // unbound.
  glBindVertexArray(vao);
  glDrawArrays(GL_TRIANGLES, 0, g_numVertices);
  SDL_GL_SwapWindow(g_pWindow);
}


int
main(int argc, char* argv[]) {
  // SDL.
  // According to the docs, SDL_Quit can be called even if SDL_Init fails.
  atexit(SDL_Quit);
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) < 0) {
    return ERR_SDL_INIT_FAILED;
  }

  // Window.
  constexpr Uint32 windowWidth = 640;
  constexpr Uint32 windowHeight = 480;
  g_pWindow = SDL_CreateWindow(
    "An SDL2 window",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    windowWidth,
    windowHeight,
    SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
  if (g_pWindow == nullptr) {
    return ERR_WINDOW_INIT_FAILED;
  }
  atexit(destroyWindow);

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
  SDL_GL_SetAttribute(
    SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  // OpenGL context.
  g_pGLContext = SDL_GL_CreateContext(g_pWindow);
  if (!g_pGLContext) {
    return ERR_GL_CONTEXT_INIT_FAILED;
  }
  atexit(destroyContext);
  if (auto const error = glGetError(); error != GL_NO_ERROR) {
    std::cerr << "GL error: " << error << std::endl;
    return ERR_GL_ERR_ON_CONTEXT_CREATION;
  }

  // GLEW initialisation.
  glewExperimental = GL_TRUE;
  if (GLenum const glewError = glewInit(); glewError != GLEW_OK) {
    std::cerr << "GLEW error: " << glewGetErrorString(glewError);
    return ERR_GLEW_INIT_FAILED;
  }

  // GL configuration.
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  if (HAS_GL_ERROR) {
    return ERR_GL_CONFIG_FAILED;
  }

  // Load GL program.
  auto const program = loadProgram(pVertShaderSrc, pFragShaderSrc);
  if (program.error) {
    std::cerr << "GL program error: " << glewGetErrorString(program.error)
      << std::endl;
    return program.error;
  }

  glUseProgram(program.handle);
  { // Set the MVP matrix (just use a simple orthographic projection).
    glm::mat4 mvp = glm::ortho(
      0.f, float(windowWidth), 0.f, float(windowHeight));
    glUniformMatrix4fv(
      glGetUniformLocation(program.handle, "mvp"), 1, GL_FALSE, 
      glm::value_ptr(mvp));
  }

  glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

  // Initialise VAO.
  GLuint vao = 0;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // Send vertex data to a buffer.
  float const xmin = 0.25f * windowWidth;
  float const xmax = 0.75f * windowWidth;
  float const ymin = 0.25f * windowHeight;
  float const ymax = 0.75f * windowHeight;
  float const vertices[2 * g_numVertices] = {
    xmin, ymax,
    xmin, ymin,
    xmax, ymin,

    xmin, ymax,
    xmax, ymin,
    xmax, ymax,
  };
  GLuint vbo = 0;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), nullptr);

  // Render.
  render(vao);

  if (char const* const pDelay = std::getenv("DELAY")) {
    int const delay = std::atoi(pDelay);
    if (delay > 0) {
      SDL_Delay(delay);
    }
  }

  return 0;
}
