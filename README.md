# Suppress
This repository contains several simple programs and build scripts for
generating Memcheck suppressions files. In particular suppressions files can be
generated for SDL/OpenGL applications.

## Usage
Everything is automated; just run the following and make a brew while you wait.
```bash
mkdir build
cd build
cmake ..
make
```

This uses CMake to bootstrap the build process; then binaries are built for
testing various bits of functionality; and finally valgrind is invoked with
each binary to generate a relevant suppressions file. The suppressions files
can then be expanded with project-specific suppressions, or used straight away
with Memcheck.

Note: while the generated suppressions files are ready to use, it's recommended
that you read them to ensure the rules are suitable.
