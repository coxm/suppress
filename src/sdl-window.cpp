#include <cstdlib>

#include <SDL2/SDL.h>


int
main() {
  SDL_Window* pWindow = nullptr;

  atexit(SDL_Quit);
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    return 1;
  }

  pWindow = SDL_CreateWindow(
    "An SDL2 window",
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    640,
    480,
    SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

  if (pWindow == nullptr) {
    return 2;
  }

  if (char const* const pDelay = std::getenv("DELAY")) {
    int const delay = std::atoi(pDelay);
    if (delay > 0) {
      SDL_Delay(delay);
    }
  }

  // Close and destroy the window
  SDL_DestroyWindow(pWindow);
  return 0;
}
